properties([
    parameters([
        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            filterable: false,
            filterLength: 0,
            name: 'Stage',
            description: 'Choose stage',
            randomName: 'choice-parameter-7601235200970',
            script: [
                $class: 'GroovyScript',
                fallbackScript: [
                    classpath: [],
                    sandbox: false,
                    script: 'return ["ERROR"]'
                ],
                script: [
                    classpath: [],
                    sandbox: false,
                    script: '''
                        return["Choose", "PRO", "PRE"]
                    '''
                ]
            ]
        ], [
            $class: 'CascadeChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            filterable: false,
            filterLength: 0,
            name: 'Branch',
            description: 'Choose branch',
            randomName: 'choice-parameter-7601235200971',
            referencedParameters: 'Stage',
            script: [
                $class: 'GroovyScript',
                fallbackScript: [
                    classpath: [],
                    sandbox: false,
                    script: 'return ["ERROR"]'
                ],
                script: [
                    classpath: [],
                    sandbox: false,
                    script: '''
                        import groovy.json.JsonSlurper;

                        def projectId = '22029839';
                        def token = 'v3EzwVnPakTc6zCs78tn';
                        def projectUrl = "https://gitlab.com/api/v4/projects/" + projectId + "/repository/branches?private_token=" + token + "&per_page=100"

                        List<String>params = new ArrayList<String>()
                        URL apiUrl = projectUrl.toURL()
                        List json = new JsonSlurper().parse(apiUrl.newReader())

                        if (Stage.equals("PRE")) {
                            params.add('Choose')

                            for (item in json) {
                                params.add(item.name + " / " + item.commit.id)
                            }

                            return params
                        } else if (Stage.equals("PRO")) {
                            params.add('Choose')

                            for (item in json) {
                                if (item.name.contains("master")) {
                                    params.add(item.name + " / " + item.commit.id)
                                }
                            }

                            return params
                        } else {
                            return ["Before you need to choose the Stage"]
                        }
                    '''
                ]
            ]
        ], [
            $class: 'CascadeChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            filterable: false,
            filterLength: 0,
            name: 'Commit',
            description: 'Choose commit',
            randomName: 'choice-parameter-7601235200972',
            referencedParameters: 'Branch',
            script: [
                $class: 'GroovyScript',
                fallbackScript: [
                    classpath: [],
                    sandbox: false,
                    script: 'return ["ERROR"]'
                ],
                script: [
                    classpath: [],
                    sandbox: false,
                    script: '''
                        import groovy.json.JsonSlurper;

                        def projectId = '22029839';
                        def token = 'v3EzwVnPakTc6zCs78tn';

                        if (Branch.contains("/")) {
                            def projectBranch = Branch.split(" / ")[0].trim()
                            def projectUrl = "https://gitlab.com/api/v4/projects/" + projectId + "/repository/commits?ref_name=" + projectBranch + "&private_token=" + token + "&per_page=100"

                            List<String>params = new ArrayList<String>()
                            URL apiUrl = projectUrl.toURL()
                            List json = new JsonSlurper().parse(apiUrl.newReader())

                            params.add('Choose')
                            params.add('Checkout of last commit')

                            for (item in json) {
                                params.add(item.short_id + " / " + item.message)
                            }

                            return params
                        } else {
                            return ["Before you need to choose the Branch"]
                        }
                    '''
                ]
            ]
        ]
    ])
])

pipeline {
    agent any

    environment {
        dockerName = 'prueba-c2'
        dockerTag = '$BUILD_NUMBER'
        dockerUsername = 'sasarafalsky'
        dockerCredentials = '0a3d0093-851a-4a02-80df-6920e8e2e55e'

        gitlabUsername = 'sasarafalsky/jenkins-docker'
        gitlabCredentials = '8d8bebd3-7080-4e87-a8ee-65d5085ec2bc'
        gitlabRepository = 'https://gitlab.com/sasarafalsky/jenkins-docker.git'

        emailReceiver = 'oleksandr.rafalskyy@mtp.es'
        nexusCredentials = '07d38783-0b4a-4a8b-b7a4-24d501ad627f'
        nexusRepository = 'jenkins_images'
    }

    stages {
        stage ('Check parameters') {
            steps {
                echo "Stage name: ${Stage}"
                echo "Branch name: ${Branch.split('\\ / ')[0].trim()}"
                echo "Commit name: ${Commit.split('\\ / ')[0].trim()}"

                sh 'whoami'
                sh 'pwd'
                sh 'ls -la'
                sh 'docker images'

                script {
                    if (!Commit.contains("/") && !Commit.contains("Checkout of last commit")) {
                        error "Bad parameters"
                    } else {
                        echo("Good parameters")
                    }
                }
            }
        }
        stage ('Checout code') {
            steps {
                deleteDir() /* clean up our workspace */

                script {
                    def branchId = Branch.split('\\ / ')[0].trim()
                    def commitId = Commit.split('\\ / ')[0].trim()

                    if (Commit.contains("Checkout of last commit")) { /* From branch*/
                        git branch: branchId,
                            credentialsId: gitlabCredentials,
                            url: gitlabRepository
                    } else if (Stage.contains("Choose") && Branch.contains("ERROR") && Commit.contains("ERROR")) { /* From cron*/
                        checkout ([$class: 'GitSCM',
                            branches: [[name: '*/master']],
                            userRemoteConfigs: [[
                                credentialsId: gitlabCredentials,
                                url: gitlabRepository
                            ]]
                        ])
                    } else if (Commit.contains("/")) { /* From commit*/
                        checkout ([$class: 'GitSCM',
                            branches: [[name: commitId ]],
                            userRemoteConfigs: [[
                                credentialsId: gitlabCredentials,
                                url: gitlabRepository
                            ]]
                        ])
                    }
                }
            }
        }
        stage ('SonarQube analysis') {
            steps {
                sh 'ls -la'

                script {
                    def scannerHome = tool 'sonarScanner'

                    /* // SonarCloud.io
                    withSonarQubeEnv('sonarCloud') {
                        sh "${scannerHome}/bin/sonar-scanner -Dsonar.organization=sasarafalsky -Dsonar.projectKey=sasarafalsky_jenkins-docker -Dsonar.projectName=${dockerName} -Dsonar.projectVersion=${dockerTag}"
                    } */

                    /* // SonarQube localhost:9004
                    withSonarQubeEnv('sonarQube') {
                        sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=Jenkins-projects -Dsonar.projectName=${dockerName} -Dsonar.projectVersion=${dockerTag}"
                    } */
                }
            }
        }
        stage("Quality Gate"){
            steps {
                echo "Qualiting..."

                /* script {
                    timeout(time: 5, unit: 'MINUTES') {
                        def qg = waitForQualityGate()

                        if (qg.status != 'OK') {
                            // error "Pipeline aborted due to quality gate failure: ${qg.status}"
                            echo "Failure: ${qg.status}"
                        } else {
                            echo "Success: ${qg.status}"
                        }
                    }
                } */
            }
        }
        stage ('Docker build') {
            steps {
                script {
                    /* // Docker Hub
                    docker.withRegistry('', dockerCredentials) {
                        def image = docker.build("${dockerUsername}/${dockerName}:0.${dockerTag}")
                        image.push()
                    } */

                    /* // Gitlab registry
                    docker.withRegistry('https://registry.gitlab.com', gitlabCredentials) {
                        def image = docker.build("${gitlabUsername}/${dockerName}:0.${dockerTag}")
                        image.push()
                    } */

                    /* // Nexus
                    docker.withRegistry('http://192.168.0.190:8089', nexusCredentials) {
                        def image = docker.build("${nexusRepository}/${dockerName}:0.${dockerTag}")
                        image.push()
                    } */

                    nexusPolicyEvaluation iqApplication: 'SampApp', iqStage: 'build'
                }
            }
        }
    }

    post {
        always {
            sh 'ls -la' /* ver el contenido de la carpeta*/
            sh 'docker images' /* listar las imagenes*/

            sh "docker images | grep ${dockerName} | awk '{print \$3}' | xargs docker rmi -f" /* borramos todas las imagenes*/
            deleteDir() /* clean up our workspace */

            sh 'ls -la' /* ver el contenido de la carpeta*/
            sh 'docker images' /* listar las imagenes*/
        }

        success {
            mail    subject: "Docker build SUCCESS, ${dockerName}:0.$BUILD_NUMBER",
                    body: """
                            <div style="width: 300px; text-align: center;">
                                <div style="font-size:24px; color:#09f; font-weight: bold;">SUCCESS</div>
                                <div style="font-size:14px; color:#000; margin: 12px 0 0;">La construcción de la imagen docker ha sido construida con éxito.</div>
                                <div style="font-size:10px; color:#666; font-weight: bold; margin: 24px 0 0;">Build version: ${dockerName}:0.$BUILD_NUMBER</div>
                            </div>""",
                    charset: 'UTF-8',
                    mimeType: 'text/html',
                    to: emailReceiver;
        }
        failure {
            mail    subject: "Docker build ERROR, ${dockerName}:0.$BUILD_NUMBER",
                    body: """
                            <div style="width: 300px; text-align: center;">
                                <div style="font-size:24px; color:#f00; font-weight: bold;">FAILURE</div>
                                <div style="font-size:14px; color:#000; margin: 12px 0 0;">Ha ocurrido un error durante la construcción de la imagen docker.</div>
                                <div style="font-size:10px; color:#666; font-weight: bold; margin: 24px 0 0;">Build version: ${dockerName}:0.$BUILD_NUMBER</div>
                            </div>""",
                    charset: 'UTF-8',
                    mimeType: 'text/html',
                    to: emailReceiver;
        }
        aborted {
            mail    subject: "Docker build ABORTED, ${dockerName}:0.$BUILD_NUMBER",
                    body: """
                            <div style="width: 300px; text-align: center;">
                                <div style="font-size:24px; color:#ffa500; font-weight: bold;">ABORTED</div>
                                <div style="font-size:14px; color:#000; margin: 12px 0 0;">Ha ocurrido un error durante la construcción de la imagen docker.</div>
                                <div style="font-size:10px; color:#666; font-weight: bold; margin: 24px 0 0;">Build version: ${dockerName}:0.$BUILD_NUMBER</div>
                            </div>""",
                    charset: 'UTF-8',
                    mimeType: 'text/html',
                    to: emailReceiver;
        }
    }
}
